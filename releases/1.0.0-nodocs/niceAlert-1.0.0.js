var na = {
	settings: {
		x: 0,
		y: 0,
		timeout: 0
	},
	makebase: (classes, ctx) => {
		ctx.container = document.createElement("div");
		ctx.container.setAttribute("class", classes);
		ctx.container.style.zindex = ctx.zindex;
		document.body.appendChild(ctx.container);
		ctx.titlebar = document.createElement("div");
		ctx.titlebar.setAttribute("class", classes + " titlebar");
		ctx.titlebar.style.zindex = ctx.zindex + 2;
		ctx.container.appendChild(ctx.titlebar);
		ctx.mainbody = document.createElement("div");
		ctx.mainbody.setAttribute("class", classes + " mainbody");
		ctx.mainbody.style.zindex = ctx.zindex + 1;
		ctx.container.appendChild(ctx.mainbody);
		ctx.bottombar = document.createElement("div");
		ctx.bottombar.setAttribute("class", classes + " bottombar");
		ctx.bottombar.style.zindex = ctx.zindex + 2;
		ctx.container.appendChild(ctx.bottombar);
	},
	alerts: [],
	alert: (title, content, callback, settings) => {na.alerts.push (new na.Alert(title, content, callback, settings, this));},
	Alert: function (title, content, callback, settings, ctx){
		this.id = na.alerts.length;
		this.zindex = this.id * 3 + 100;
		na.makebase("alert-all alert-alert", this);
		this.titlebar.innerHTML = title
		this.mainbody.innerHTML=content;
		this.button = document.createElement("input");
		this.bottombar.appendChild(this.button);
		this.button.setAttribute("type", "button");
		this.button.setAttribute("value","OK");
		this.button.setAttribute("class", "alert-all alert-alert button");
		this.button.addEventListener("click", ()=>{
			try{
				callback.bind(ctx)();
			} finally {
				this.container.parentNode.removeChild(this.container);
			}
		});
	},
	choose: (title, content, buttons, settings) => {na.alerts.push (new na.Choose(title, content, buttons, settings, this));},
	Choose: function (title, content, buttons, settings, ctx){
		this.id = na.alerts.length;
		this.zindex = this.id * 3 + 100;
		na.makebase("alert-all alert-choose", this);
		this.titlebar.innerHTML = title
		this.mainbody.innerHTML=content;
		this.button = [];
		for (let i = 0; i < buttons.length; i++){
			this.button[i] = document.createElement("input");
			this.bottombar.appendChild(this.button[i]);
			this.button[i].setAttribute("type", "button");
			this.button[i].setAttribute("value",buttons[i].text);
			this.button[i].setAttribute("class", "alert-all alert-choose button");
			this.button[i].addEventListener("click", ()=>{
				try{
					buttons[i].callback.bind(ctx)();
				} finally {
					this.container.parentNode.removeChild(this.container);
				}
			});
		}
	},
	input: (title, content, successCallback, failCallback, enterOK, settings) => {na.alerts.push (new na.Input(title, content, successCallback, failCallback, enterOK, settings, this));},
	Input = function (title, content, successCallback, failCallback, enterOK, settings, ctx){
		this.id = na.alerts.length;
		this.zindex = this.id * 3 + 100;
		na.makebase("alert-all alert-alert", this);
		this.titlebar.innerHTML = title
		this.mainbody.innerHTML=content;
		this.textinput = document.createElement("input");
		this.bottombar.appendChild(this.textinput);
		this.textinput.setAttribute("type", "text");
		this.textinput.setAttribute("value","");
		this.textinput.setAttribute("class", "alert-all alert-alert textinput");
		if (enterOK == true || enterOK == undefined){
			this.textinput.addEventListener("keydown", e => {
				if (e.key == "Enter"){
					try{
						successCallback.bind(ctx)(this.textinput.value);
					} finally {
						this.container.parentNode.removeChild(this.container);
					}
				};
			});
		}
		this.failButton = document.createElement("input");
		this.bottombar.appendChild(this.failButton);
		this.failButton.setAttribute("type", "button");
		this.failButton.setAttribute("value","Cancel");
		this.failButton.setAttribute("class", "alert-all alert-alert button");
		this.failButton.addEventListener("click", ()=>{
			try{
				failCallback.bind(ctx)();
			} finally {
				this.container.parentNode.removeChild(this.container);
			}
		});
		this.successButton = document.createElement("input");
		this.bottombar.appendChild(this.successButton);
		this.successButton.setAttribute("type", "button");
		this.successButton.setAttribute("value","OK");
		this.successButton.setAttribute("class", "alert-all alert-alert button");
		this.successButton.addEventListener("click", ()=>{
			try {
				successCallback.bind(ctx)(this.textinput.value);
			} finally {
				this.container.parentNode.removeChild(this.container);
			}
		});
	}
};
