na.choose = (title, content, buttons, settings) => {na.alerts.push (new na.Choose(title, content, buttons, settings, this));};

na.Choose = function (title, content, buttons, settings, ctx){
	this.id = na.alerts.length;
	this.zindex = this.id * 3 + 100;
	na.makebase("alert-all alert-choose", this);
	this.titlebar.innerHTML = title
	this.mainbody.innerHTML=content;
	this.button = [];
	for (let i = 0; i < buttons.length; i++){
		this.button[i] = document.createElement("input");
		this.bottombar.appendChild(this.button[i]);
		this.button[i].setAttribute("type", "button");
		this.button[i].setAttribute("value",buttons[i].text);
		this.button[i].setAttribute("class", "alert-all alert-choose button");
		this.button[i].addEventListener("click", ()=>{
			try{
				buttons[i].callback.bind(ctx)();
			} finally {
				this.container.parentNode.removeChild(this.container);
			}
		});
	}
};
